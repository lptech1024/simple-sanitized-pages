console.log("Now");

everything();

const targetNodeMode = [ "elementText", "elementRemove" ];

var targetNodesWithoutObservers = 0;
const targetNodes =
[
	{ id : targetNodesWithoutObservers++, mode : targetNodeMode.element, existId : "accountMask", selector : "span#accountMask", target : null, innerText : "HELLO", observer : null }
];

var targetNode = document.querySelector("span#accountMask");
//const config = { attributes: true };
//const config = { characterData: true };
const config = { childList: true,
                 attributeFilter: [ "text" ]
               };

const callback = function(mutationsList, observer)
{
	mutationsList.forEach(mutation =>
		{
			console.log("Mutation");
			//console.dir(mutation);
			// Using traditional for loop for maximum performance
			for (let i = 0; i < targetNodes.length; i++)
			{
				let targetNode = targetNodes[i];
				if (mutation.target == targetNode.target)
				{
					targetNode.observer.disconnect();
					targetNode.target.innerText = targetNode.innerText;
					targetNode.observer.observe(targetNode.target, config);
					break;
				}
			}
		});
};

function replaceAndWatchForChanges(targetNode, startup)
{
	console.log("Hello");
	new MutationObserver(function(mutations)
	{
		let nodeList = null;
		//console.log("Next line fails");
		console.log(`targetNode.selector: ${targetNode.selector}`);
		let element = document.getElementById("accountMask");
		if (element)
		{
			/*if (targetNode.mode === targetNodeMode.elements)
			{
				nodeList = document.querySelectorAll();
			}*/

			if (nodeList)
			{
				console.log("TODO: Finish nodeList");
			}
			else
			{
				console.log("Working element...");
				if (targetNode.innerText)
				{
					console.log(`Setting innerText: ${targetNode.innerText}`);
					this.disconnect();
					element.innerText = targetNode.innerText;
				}
			}
		}
		else
		{
			console.log("No element");
		}
	}).observe(document.querySelector("span#accountMask"), config);
	console.log("Goodbye");
}

function testing()
{
	new MutationObserver(function(mutations)
	{
		// Need to account for each potentially-relevant mutation due to inner short-circuit logic
		mutations.forEach(mutation =>
			{
				// Using traditional for loop for maximum performance
				for (let i = 0; i < targetNodes.length; i++)
				{
					let targetNode = targetNodes[i];
					if (!targetNode.observer)
					{
						if (document.getElementById(targetNode.existId))
						{
							targetNode.observer = new MutationObserver(callback);
							// TODO: If appropriate mode
							let element = document.querySelector(targetNode.selector);
							if (element)
							{
								element.innerText = targetNode.innerText;
							}
							else
							{
								console.log(`Missing element for ${targetNode.existId}`);
							}

							targetNode.target = element;
							targetNode.observer.observe(element, config);
							targetNodesWithoutObservers--;
							console.log(`Target nodes without observers ${targetNodesWithoutObservers}`);
							if (targetNodesWithoutObservers <= 0)
							{
								//console.log("Disconnecting"); //DEBUG
								this.disconnect();
							}

							break;
						}
					}
				}
			});
		
	}).observe(document,
		{
			subtree: true,
			childList: true
		});
}

// TODO: Use
function changeInnerTextIfValid(element, text)
{
	if (element)
	{
		element.innerText = text;
	}
}

function everything()
{
	//console.log("Then");
	// Remove cluttering elements
	document.querySelector("div#site-message-container")?.remove();
	document.querySelector("div#specialAnnouncementContainer")?.remove();
	document.querySelector("div.account-reward-tile.has-credit-score-tile[role='link'][tabindex='0']")?.remove();
	document.querySelector("div#mainAccountcreditScore.account-tile-section")?.remove();
	document.querySelector("div#summaryAdContainer")?.remove();
	document.querySelector("div#logonDetailsContainer span.NOTE")?.remove();
	document.querySelector("div.ads-tiles")?.remove();

	var creditCardMask = ['0123', '4567', '8901', '2345', '6789', '0000', '1111', '2222', '3333', '4444', '5555', '6666', '7777', '8888', '9999'];
	var maxMaskIndex = creditCardMask.length - 1;
	var currentMaskIndex = 0;

	var accountTiles = document.querySelectorAll("section#accountTiles.account-tiles-section div.account-tile");
	if (accountTiles)
	{
		/*console.log("Account tiles");
		console.dir(accountTiles);*/
		accountTiles.forEach(accountTile =>
			{
				var shortAccountMaskText = `(..${creditCardMask[currentMaskIndex]})`;
				var longAccountMaskText = `(...${creditCardMask[currentMaskIndex]})`;

				// TODO: Rename to selectedCurrentBalance
				let currentBalance = "$0.00";

				if (accountTile.classList.contains("active"))
				{
					let element = document.querySelector("span#accountMask")
					if (element)
					{
						element.innerText = longAccountMaskText;
					}

					//console.log("Offer head...");
					var offerHeader = document.querySelector("div#cardlyticsOfferCarousalContentID div.mktuiHeading.accountInfo");
					if (offerHeader)
					{
						let offerText = offerHeader.innerHTML.split("(..");
						if (offerText)
						{
							offerHeader.innerText = `${offerText[0]} ${shortAccountMaskText}`;
						}
					}

					document.querySelector("div#noPaymentReqdMessage")?.remove();
					document.querySelector("div#cashBack")?.remove();

					var topRightPanelAccountBalance = document.querySelector("span#accountCurrentBalanceWithToolTipValue");
					if (topRightPanelAccountBalance)
					{
						topRightPanelAccountBalance.innerText = currentBalance;
					}

					var topRightPanelAvailableCredit = document.querySelector("span#availableCreditWithTransferBalanceValue");
					if (topRightPanelAvailableCredit)
					{
						topRightPanelAvailableCredit.innerText = currentBalance;
					}

					var topRightPanelMinimumAmountDue = document.querySelector("span#minimumAmountDueValue");
					if (topRightPanelMinimumAmountDue)
					{
						topRightPanelMinimumAmountDue.innerText = currentBalance;
					}

					var topRightPanelStatementBalance = document.querySelector("span#remainingStatementBalanceValue");
					if (topRightPanelStatementBalance)
					{
						topRightPanelStatementBalance.innerText = currentBalance;
					}

					var topRightPanelNextDueDate = document.querySelector("span#nextPaymentDueDateValue");
					if (topRightPanelNextDueDate)
					{
						topRightPanelNextDueDate.innerText = "";
					}

					var pendingPanelCharges = document.querySelector("div#activityContainerpendingActivity div.pending-total dd.pendingcharge-value");
					if (pendingPanelCharges)
					{
						pendingPanelCharges.innerText = currentBalance;
					}

					var pendingElements = document.querySelectorAll("table#activityTablependingActivity tbody tr");
					if (pendingElements)
					{
						pendingElements.forEach(pendingElement =>
							{
								let pendingElementDate = pendingElement.querySelector("td.date span");
								if (pendingElementDate)
								{
									pendingElementDate.innerText = "";
								}

								let pendingElementDescription = pendingElement.querySelector("td.description > span");
								if (pendingElementDescription)
								{
									pendingElementDescription.innerText = "Something Embarrassing";
								}

								let pendingElementAmount = pendingElement.querySelector("td.amount span");
								if (pendingElementAmount)
								{
									pendingElementAmount.innerText = currentBalance;
								}
							});
					}

					var activityPanelAccountBalance = document.querySelector("div#activityContainerdashboardEtdActivity div.current-balance-section dd.BODYLABEL");
					if (activityPanelAccountBalance)
					{
						activityPanelAccountBalance.innerText = currentBalance;
					}

					var activityElements = document.querySelectorAll("table#activityTabledashboardEtdActivity tbody tr");
					if (activityElements)
					{
						/*console.log("Activity Elements");
						console.dir(activityElements);*/
						activityElements.forEach(activityElement =>
							{
								console.log("Activity Element");
								let activityElementDate = activityElement.querySelector("td.date span");
								if (activityElementDate)
								{
									activityElementDate.innerText = "";
								}

								let activityElementDescription = activityElement.querySelector("td.description div span");
								if (activityElementDescription)
								{
									activityElementDescription.innerText = "Something Embarrassing";
								}

								// Can be span or link
								let activityElementCategory = activityElement.querySelector("td.category span")?.childNodes[0];
								if (activityElementCategory)
								{
									activityElementCategory.innerText = "Personal";
								}

								let activityElementAmount = activityElement.querySelector("td.amount span");
								if (activityElementAmount)
								{
									activityElementAmount.innerText = currentBalance;
								}
							});
					}
				}

				accountTile.querySelector("span.mask-number").innerHTML = longAccountMaskText;
				accountTile.querySelector("div.balance-block span.balance").innerHTML = currentBalance;

				if (currentMaskIndex < maxMaskIndex)
				{
					currentMaskIndex++;
				}
			});
	}
}
